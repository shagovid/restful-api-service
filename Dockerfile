FROM centos:7

RUN yum update -y && yum install -y gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel wget make
RUN wget https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz && tar xzf Python-3.7.11.tgz && \
    cd Python-3.7.11  && ./configure --enable-optimizations && make altinstall && rm -rf Python-3.7.11*
RUN ln -s /usr/local/bin/python3.7 /usr/bin/python3 && ln -s /usr/local/bin/pip3.7 /usr/bin/pip3
RUN python3 -V
RUN pip3 install flask flask_restful flask_jsonpify
ADD python-api.py /python_api/
ENTRYPOINT ["python3", "/python_api/python-api.py"]
